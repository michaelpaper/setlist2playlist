# Setlist2Playlist

An open source command-line tool to create a Spotify playlist that contains the
set of all songs played live by a given band recently.

To do so, this tool relies on
[the API of setlist.fm](https://api.setlist.fm/docs/1.0/ui/index.html#//1.0/)
and on [the API of Spotify](https://developer.spotify.com/console/). Check out
those links to find how to get api keys for those websites.
Then put those keys in files names `apikey_setlist.txt` and
`apikey_spotify.txt`.
For the Spotify API key, make sure that your key has the right permissions,
namely `playlist-modify-public` and `playlist-modify-private`.

To use the script, simply run

```sh
python3 setlist2playlist.py <band> [hint]
```

For instance, to create a playlist with all the songs played live recently by
Jethro Tull, run this :

```sh
python3 setlist2playlist.py "Jethro Tull"
```

Multiple bands may have names very close to each other and it can make it hard
for the script to know which band's setlists to fetch.
In case the script can't pick the right band, you can give it a hint regarding
the band's genre.
For instance the following command will create a playlist with songs from a
rapper:

```sh
python3 setlist2playlist.py Exodus
```

But this command will create a setlist will create a playlist with songs from
a thrash metal band:

```sh
python3 setlist2playlist.py Exodus Thrash
```


## Licence

This project is licensed under GPL license, see the [LICENSE](LICENSE) file
for details.
